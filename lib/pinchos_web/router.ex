defmodule PinchosWeb.Router do
  use PinchosWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PinchosWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/contests", ContestController
  end

  # Other scopes may use custom stacks.
  # scope "/api", PinchosWeb do
  #   pipe_through :api
  # end
end
