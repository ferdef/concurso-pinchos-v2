defmodule PinchosWeb.PageController do
  use PinchosWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
