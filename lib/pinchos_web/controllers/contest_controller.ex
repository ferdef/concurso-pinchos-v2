defmodule PinchosWeb.ContestController do
  use PinchosWeb, :controller

  alias Pinchos.Global
  alias Pinchos.Global.Contest

  def index(conn, _params) do
    contests = Global.list_contests()
    render(conn, "index.html", contests: contests)
  end

  def new(conn, _params) do
    changeset = Global.change_contest(%Contest{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"contest" => contest_params}) do
    case Global.create_contest(contest_params) do
      {:ok, contest} ->
        conn
        |> put_flash(:info, "Contest created successfully.")
        |> redirect(to: Routes.contest_path(conn, :show, contest))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    contest = Global.get_contest!(id)
    render(conn, "show.html", contest: contest)
  end

  def edit(conn, %{"id" => id}) do
    contest = Global.get_contest!(id)
    changeset = Global.change_contest(contest)
    render(conn, "edit.html", contest: contest, changeset: changeset)
  end

  def update(conn, %{"id" => id, "contest" => contest_params}) do
    contest = Global.get_contest!(id)

    case Global.update_contest(contest, contest_params) do
      {:ok, contest} ->
        conn
        |> put_flash(:info, "Contest updated successfully.")
        |> redirect(to: Routes.contest_path(conn, :show, contest))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", contest: contest, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    contest = Global.get_contest!(id)
    {:ok, _contest} = Global.delete_contest(contest)

    conn
    |> put_flash(:info, "Contest deleted successfully.")
    |> redirect(to: Routes.contest_path(conn, :index))
  end
end
