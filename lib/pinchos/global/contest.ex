defmodule Pinchos.Global.Contest do
  use Ecto.Schema
  import Ecto.Changeset

  schema "contests" do
    field :active, :boolean, default: false
    field :contest_date, :date
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(contest, attrs) do
    contest
    |> cast(attrs, [:name, :contest_date, :active])
    |> validate_required([:name, :contest_date, :active])
  end
end
