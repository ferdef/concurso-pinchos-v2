defmodule Pinchos.Repo do
  use Ecto.Repo,
    otp_app: :pinchos,
    adapter: Ecto.Adapters.Postgres
end
