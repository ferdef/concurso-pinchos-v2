defmodule Pinchos.Repo.Migrations.CreateContests do
  use Ecto.Migration

  def change do
    create table(:contests) do
      add :name, :string
      add :contest_date, :date
      add :active, :boolean, default: false, null: false

      timestamps()
    end

  end
end
