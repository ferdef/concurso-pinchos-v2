defmodule Pinchos.GlobalTest do
  use Pinchos.DataCase

  alias Pinchos.Global

  describe "contests" do
    alias Pinchos.Global.Contest

    @valid_attrs %{active: true, contest_date: ~D[2010-04-17], name: "some name"}
    @update_attrs %{active: false, contest_date: ~D[2011-05-18], name: "some updated name"}
    @invalid_attrs %{active: nil, contest_date: nil, name: nil}

    def contest_fixture(attrs \\ %{}) do
      {:ok, contest} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Global.create_contest()

      contest
    end

    test "list_contests/0 returns all contests" do
      contest = contest_fixture()
      assert Global.list_contests() == [contest]
    end

    test "get_contest!/1 returns the contest with given id" do
      contest = contest_fixture()
      assert Global.get_contest!(contest.id) == contest
    end

    test "create_contest/1 with valid data creates a contest" do
      assert {:ok, %Contest{} = contest} = Global.create_contest(@valid_attrs)
      assert contest.active == true
      assert contest.contest_date == ~D[2010-04-17]
      assert contest.name == "some name"
    end

    test "create_contest/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Global.create_contest(@invalid_attrs)
    end

    test "update_contest/2 with valid data updates the contest" do
      contest = contest_fixture()
      assert {:ok, %Contest{} = contest} = Global.update_contest(contest, @update_attrs)
      assert contest.active == false
      assert contest.contest_date == ~D[2011-05-18]
      assert contest.name == "some updated name"
    end

    test "update_contest/2 with invalid data returns error changeset" do
      contest = contest_fixture()
      assert {:error, %Ecto.Changeset{}} = Global.update_contest(contest, @invalid_attrs)
      assert contest == Global.get_contest!(contest.id)
    end

    test "delete_contest/1 deletes the contest" do
      contest = contest_fixture()
      assert {:ok, %Contest{}} = Global.delete_contest(contest)
      assert_raise Ecto.NoResultsError, fn -> Global.get_contest!(contest.id) end
    end

    test "change_contest/1 returns a contest changeset" do
      contest = contest_fixture()
      assert %Ecto.Changeset{} = Global.change_contest(contest)
    end

    test "setting one contest as active makes the rest inactive" do
      contest_fixture()
      contest_fixture(name: "active Contest")
      contest_fixture(active: false)
      assert 3 = Enum.count(Global.list_contests())
      assert %Contest{name: "active Contest"} =  Global.get_active_contest()
    end
  end
end
